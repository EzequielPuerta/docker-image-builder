#!/usr/bin/env bash
set -eu

function help {
  echo "USAGE: $0 <docker_tag> <image_dir>"
  exit 1
}

if [ "$#" -ne 2 ]
then
  help
else
  DOCKER_TAG="$1"
  IMAGE_DIR="$2"
  IMAGE_NAME="${DOCKER_TAG}_image.tar"

  docker build -t "${DOCKER_TAG}" "${IMAGE_DIR}"
  docker save "${DOCKER_TAG}" > "${IMAGE_NAME}"
  gzip "${IMAGE_NAME}"
  exit 0
fi
