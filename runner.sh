#!/usr/bin/env bash
set -eu

function help {
  echo "USAGE: $0 <image_tar_gz> <image_tag>"
  exit 1
}

if [ "$#" -ne 2 ]
then
  help
else
  gzip --decompress --stdout "$1" | docker load
  docker run --rm -it "$2"
fi
